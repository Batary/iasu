#pragma once
#include <BWAPI.h>

#include <BWTA.h>
#ifdef _WIN32
#include <Windows.h>
#else
#ifndef DWORD
#define WINAPI
typedef unsigned long DWORD;
#endif
#endif

#include <string>
#include <sstream>
#include <ctime>
#include <list>
#include <vector>
#include <map>


#include <iostream>
#include <fstream>
#include <algorithm>
#include <math.h>

#include "WorkerManager.h"
#include "ProductionManager.h"
#include "BuildingManager.h"
#include "InformationManager.h"
#include "AttackManager.h"
#include "StrategyManager.h"
#include "TacticManager.h"

#include "Common.h"
#include "TimeManager.cpp"

extern bool analyzed;
extern bool analysis_just_finished;
extern BWTA::Region* home;
extern BWTA::Region* enemy_base;
DWORD WINAPI AnalyzeThread();


using namespace BWAPI;
using namespace std;



class Overkill : public BWAPI::AIModule
{
	void			readHistoryFile();
	void			writeCurrentPlay(bool isWin);
	
	openingStrategy						chooseOpeningStrategy;
	std::vector<std::vector<string>>	historyInfo;
	std::string winRate;
	

public:
	
	virtual void onStart();
	virtual void onEnd(bool isWinner);
	virtual void onFrame();
	virtual void onSendText(std::string text);
	virtual void onNukeDetect(BWAPI::Position target);
	virtual void onUnitDiscover(BWAPI::Unit unit);
	virtual void onUnitEvade(BWAPI::Unit unit);
	virtual void onUnitShow(BWAPI::Unit unit);
	virtual void onUnitHide(BWAPI::Unit unit);
	virtual void onUnitCreate(BWAPI::Unit unit);
	virtual void onUnitDestroy(BWAPI::Unit unit);
	virtual void onUnitMorph(BWAPI::Unit unit);
	virtual void onUnitComplete(BWAPI::Unit unit);
	
	virtual void onReceiveText(BWAPI::Player player, std::string text);
	virtual void onPlayerLeft(BWAPI::Player player);
	virtual void onUnitRenegade(BWAPI::Unit unit);
	virtual void onSaveGame(std::string gameName);
	// Everything below this line is safe to modify.


	void    drawTerrainData();
	void	drawStats(); //not part of BWAPI::AIModule
	void	drawBullets();
	void	drawVisibilityData();
	void	drawPaths();

	void	showPlayers();
	void	showForces();

	bool show_bullets;
	bool show_visibility_data;
	bool show_paths;
};



