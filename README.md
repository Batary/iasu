IASU is a Starcraft:Broodwar bot that uses NEAT with novelty search to learn how to play.
Before training it does nothing but mining with the 4 initial workers.
This program uses code from https://github.com/jal278/novelty-search-cpp. It has been designed to be able to work with multiple instances learning at the same time.

To use on windows, you just need to copy the dll from IASUModule as usual with BWAPI. There are no dependencies.
To use on linux, you need to install OPENBW with BWAPI (see https://github.com/OpenBW/bwapi) and copy this repository on top of the BWAPI folder.

The main source code (out of novelty search) is in the IASUGameWrapper.cpp file. To run this bot you need to have bwapi-data/read and bwapi-data/write folders existing, with, in read folder :
- startgenes and neatsettings.ne existing and well defined for training OR
- best file along with "#define BEST_MODE" uncommented to get the best individuals so far.
